#Script to recalculate PSY-1 Water Potential data based on user-defined sensor head calibration
#copies existing columns Water Potential, Slope, Intercept to new columns "MNOrig..." and replaces the originals with newly entered/calculated values
#full script and documentation: https://bitbucket.org/ponycopter/psy-posthoc-calib
#written in 2017 by Markus Nolf (m.nolf@westersydney.edu.au).
#see line 85 and following for usage examples

#function to read info from the first 15 lines of the csv file
getinfo <- function(origfile) {
  header <- read.table(origfile,header=FALSE,nrows=15,sep="|", stringsAsFactors=FALSE, comment.char="@")
  paste(header[c(1,3,6,7),])
}
#function to apply new calibration values (intercept, slope) to an existing file. defaults to "standard calibration" if values are not provided.
applyPSYcalib <- function(origfile,newintercept=1,newslope=-3.8){
  
  #read file header
  header <- read.table(origfile,header=FALSE,nrows=15,sep="|", stringsAsFactors=FALSE, comment.char="@")
  header
  
  #read file data
  data <- read.csv(origfile, skip=15, stringsAsFactors=FALSE)
  head(data)
  #fix mu-encoding issue in column names
  colnames(data)[4:5] <- c("dT..uV.","Wet.Bulb.Depression..uV.")
  
  
  #save original values to new columns
  data$MNOrigIntercept <- data$Intercept
  data$MNOrigSlope <- data$Slope
  data$MNOrigCorrWP <- data$Corrected.Water.Potential..MPa.
  
  #replace old calibration values with new values
  data$Intercept <- newintercept
  data$Slope <- newslope
  
  #generate correction factors (uses the log-function from correctionfactors.r) from chamber temperature
  data$MNCorrectionFactor <- 9.128892003*exp(-0.004283946*as.double(data$Chamber.Temperature...C.))
  
  #get parameters C1 and C2 (device/sensor-head specific) from csv header
  headSerial <- as.numeric(strsplit(header[3,],",")[[1]][2])
  data$MNC1 <- as.numeric(strsplit(header[14,],",")[[1]][2])
  data$MNC2 <- as.numeric(strsplit(header[15,],",")[[1]][2])
  
  #calculate new water potential from new calibration parameters
  #formula from psy1 manual v4.4, page 23:
  #PSI = ((((WBD/((C1*Tc)+C2))-CI)/-CS)+deltaT/k*CF))
  data$MNNewWaterPotential <- round(((((as.numeric(data$Wet.Bulb.Depression..uV.)/((data$MNC1*as.numeric(data$Chamber.Temperature...C.))+data$MNC2))-as.numeric(data$Intercept))/as.numeric(data$Slope))+(as.numeric(data$dT..uV.)/61*data$MNCorrectionFactor)),2)
  
  #make sure faulty measurements (chamber temperature > 100 degrees C) is set to NA
  #this happens e.g. when the sensor head is disconnected from the logger, which keeps happily logging
  data[!is.na(as.numeric(data$Chamber.Temperature...C.)) & as.numeric(data$Chamber.Temperature...C.)>100,]["MNNewWaterPotential"] <- NA
  
  data$Corrected.Water.Potential..MPa. <- data$MNNewWaterPotential
  head(data,20)
  
  #calculation quality control: run diagnostics when using the same slope and intercept as in the original
  if(mean(na.omit(as.numeric(data$MNOrigSlope)))==newslope & mean(na.omit(as.numeric(data$MNOrigIntercept)))==newintercept) {
    data$MNDiff <- data$Corrected.Water.Potential..MPa.-as.numeric(data$MNOrigCorrWP)
    plot(data$MNOrigCorrWP,data$MNDiff)
    plot(data$Chamber.Temperature...C.,data$MNDiff, xlim=c(0,50))
    
    print(paste("number of rows:",nrow(data)))
    
    meandiff <- sum(abs(na.omit(data$MNDiff)))/nrow(data)
    print(paste("mean difference across all rows:",round(meandiff,5)))
    
    maxdiff <- max(abs(na.omit(data$MNDiff)))
    print(paste("maximum difference:",round(maxdiff,3)))
    print("if this is >0.01, double-check the rows in question!") 
    #data[!is.na(abs(data$MNDiff)) & abs(data$MNDiff)>0.1,]
    
  }
  
  plot(data$MNOrigCorrWP,type="l", main=paste(origfile,paste("head#",headSerial),sep="\n"), ylim=range(c(na.omit(as.numeric(data$MNOrigCorrWP)),na.omit(data$MNNewWaterPotential))), xlab="Row Index", ylab="Water Potential (MPa)")
  lines(data$MNNewWaterPotential, col=2)
  text(c(10,10),c(0,-0.5),c(paste("old calibration (s ",mean(na.omit(as.numeric(data$MNOrigSlope))),", i ",mean(na.omit(as.numeric(data$MNOrigIntercept))),")",sep=""),paste("new calibration (s ",newslope,", i ",newintercept,")",sep="")),col=c(1,2),adj=0)
  newfilename <- paste(substr(origfile,0,(nchar(origfile)-4)),"-newcalib.csv",sep="")
  
  write.table(header,file=newfilename,quote=FALSE,col.names=FALSE,row.names=FALSE,sep=","); write.table(data,file=newfilename,append=TRUE,row.names=FALSE,quote=FALSE,sep=",")
  print(paste("new file saved:",newfilename))
  
  output <- list(header=header,data=data)
  
  return(output)
}



#usage examples

#set working directory to where your PSY-1 csv file is 
#setwd("c:\\test")

#print some header information from the top of the file
getinfo("testdata.csv")

#if desired, double-check calculation of WP with default calibration
#output <- applyPSYcalib("testdata.csv")

#apply new intercept and slope values from device calibration 
#generates a new csv-file [previousname]-newcalib.csv in the working directory
output <- applyPSYcalib("testdata.csv",newintercept=1.029,newslope=-3.35) # logger 5, head 1965 (calibrated 160516)

output$header #original PSY file header
head(output$data) #complete data table to play with in R: output$data
