# README #

This is an R-script to recalculate PSY-1 Water Potential data based on user-defined sensor head calibration. 
It may be useful to fix WP data recorded with standard sensor-head calibration, or where the datalogger has lost its calibration.


Version 1.0 (20170817)


Code files:

correctionfactors.r is used to reconstruct and precisely calculate PSY-1 correction factors, as taken from page 154 in the ICT Stem Psy Manual (v. 4.4, revision 17/04/2013). Results from this script are already included in recalculate-psy1wp.r, so this is just included for retraceability.


recalculate-psy1wp.r opens a user-specified WP-datafile (csv), copies existing columns Water Potential, Slope, Intercept to new columns "MNOrig[previousname]", and replaces the originals with newly entered/calculated values.



Written in 2017 by Markus Nolf (m.nolf@westersydney.edu.au).


### Usage examples ###
These are also included in recalculate-psy1wp.r.

Set working directory to where your PSY-1 csv file is 
~~~~
setwd("c:\\test")
~~~~

Print some header information from the top of the file
~~~~
getinfo("testdata.csv")
~~~~

If desired, double-check calculation of WP with default calibration
~~~~
output <- applyPSYcalib("testdata.csv")
~~~~

Apply new intercept and slope values from device calibration.

This generates a new csv-file [previousname]-newcalib.csv in the working directory
~~~~
output <- applyPSYcalib("testdata.csv",newintercept=1.029,newslope=-3.35)
~~~~

Print original PSY file header
~~~~
output$header
~~~~

Access the complete data table to play with in R: 
~~~~
output$data
~~~~
