#Script to reconstruct and precisely calculate PSY-1 correction factors
#correction factors in the csv as taken from page 154 in the ICT Stem Psy Manual (v. 4.4, revision 17/04/2013)
#full script and documentation: https://bitbucket.org/ponycopter/psy-posthoc-calib
#written in 2017 by Markus Nolf (m.nolf@westersydney.edu.au).

cf <- read.csv("correctionfactors.csv", comment.char="#")
head(cf)

if(FALSE) {
  #fit linear correlation to correction factors
  lincorr <- lm(CF~Temp,data=cf)
  with(cf, plot(Temp,CF,pch=1)); 
  abline(lincorr, col=2, lwd=2)
  lincorr
  summary(lincorr)
  coef(lincorr)
  # a = 9.07786445
  # b = -0.03475712
  
  #test fit quality by re-calculating correction factors
  cf$test <- 9.07786445-cf$Temp*0.03475712
  cf[cf$Temp==20,]
  cf[cf$Temp==25,]
  cf[cf$Temp==30,]
}

#instead, fit a log function to correction factors
y=cf$CF
x=cf$Temp
corr2 <- nls(y~a*exp(-b*x),start=list(a=9,b=0.1))
summary(corr2)
with(cf,plot(Temp,CF))
lines(cf$Temp,predict(corr2,list(Temp=cf$Temp)), col=2, lwd=2)

corr2 
coef(corr2)
# a = 9.128892003
# b = 0.004283946

cf$test <- 9.128892003*exp(-0.004283946*cf$Temp)
#test fit quality by re-calculating correction factors
cf[cf$Temp==20,]
cf[cf$Temp==25,]
cf[cf$Temp==30,]


#check fit quality: 
plot(cf$CF,cf$test, pch=16, cex=0.1)
lines(c(5,10),c(5,10), col=2, lty=2, lwd=2)
cor(cf$CF,predict(corr2))
mean(cf$CF-cf$test) #mean difference between table value and reconstructed value
max(cf$CF-cf$test) #max difference between table value and reconstructed value
sum(cf$CF-cf$test) #sum of all differences between table values and reconstructed values
nrow(cf)
#0.0012 MPa total difference across 350 rows
#minimal deviation from table; should be OK to calculate WP at 2 postcomma-digits.



#use this formula to calculate correction factors (from line 40 in this script): 
cf$test <- 9.128892003*exp(-0.004283946*cf$Temp)
